import { Injectable } from '@angular/core';
import { Product } from './product.model';
import { Observable, BehaviorSubject, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productsSubject: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
  products$: Observable<Product[]> = this.productsSubject.asObservable();

  constructor(private http: HttpClient) {
    this.loadProducts();
  }

  private loadProducts() {
    this.http.get<{ data: Product[] }>('assets/products.json').subscribe(
      (data) => {
        this.productsSubject.next(data.data);
      },
      (error) => {
        console.error('Error loading products:', error);
      }
    );
  }

  getProducts(): Observable<Product[]> {
    return this.products$;
  }

  addProduct(newProduct: Product): void {
    const currentProducts = this.productsSubject.value;
    this.productsSubject.next([...currentProducts, newProduct]);
  }

  updateProduct(updatedProduct: Product): void {
    const currentProducts = this.productsSubject.value;
    const index = currentProducts.findIndex((p) => p.id === updatedProduct.id);
    if (index !== -1) {
      currentProducts[index] = updatedProduct;
      this.productsSubject.next([...currentProducts]);
    }
  }

  deleteProduct(productId: number): void {
    const currentProducts = this.productsSubject.value;
    this.productsSubject.next(currentProducts.filter((p) => p.id !== productId));
  }

  applyFilters(products: Product[], nameFilter: string, categoryFilter: string, ratingFilter: string): Product[] {
    return products.filter(product =>
      (!nameFilter || product.name === nameFilter) &&
      (!categoryFilter || product.category === categoryFilter) &&
      (!ratingFilter || product.rating.toString() === ratingFilter)
    );
  }

  getFilteredProducts(nameFilter: string, categoryFilter: string, ratingFilter: string): Observable<Product[]> {
    return this.getProducts().pipe(
      map(products => this.applyFilters(products, nameFilter, categoryFilter, ratingFilter))
    );
  }


}
