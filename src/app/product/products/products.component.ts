import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product.model';
import { Observable } from 'rxjs';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products$: Observable<Product[]>;

  layoutOptions: SelectItem[];
  selectedLayout: string;

  sortOptions: SelectItem[];
  selectedSort: string;

  searchText: string = '';
  filteredProducts: Product[] = [];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.products$ = this.productService.getProducts();

    this.layoutOptions = [
      { label: 'List', value: 'list' },
      { label: 'Grid', value: 'grid' },
    ];

    this.selectedLayout = 'list';

    this.sortOptions = [
      { label: 'Name', value: 'name' },
      { label: 'Price', value: 'price' },
    ];

    this.selectedSort = 'name';

    this.applySearch();
  }

  applySearch() {
    this.products$.subscribe(products => {
      this.filteredProducts = products.filter(product =>
        product.name.toLowerCase().includes(this.searchText.toLowerCase())
      );
    });
  }

}