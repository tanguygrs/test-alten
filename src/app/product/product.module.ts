import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { ProductsAdminComponent } from './products-admin/products-admin.component';
import { DataViewModule } from 'primeng/dataview';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'primeng/api';



@NgModule({
  declarations: [
    ProductsComponent,
    ProductsAdminComponent,
  ],
  imports: [
    CommonModule,
    DataViewModule,
    DropdownModule,
    ButtonModule,
    FormsModule,
    SharedModule
  ]
})
export class ProductModule { }
